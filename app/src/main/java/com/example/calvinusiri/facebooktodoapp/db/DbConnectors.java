package com.example.calvinusiri.facebooktodoapp.db;

import android.provider.BaseColumns;

public class DbConnectors {
    // Providers the connector credencials for the DB
    public static final String DB_NAME = "tasks_db";// Name of the Db
    public static final int DB_VERSION = 1;

    public class TaskEntry implements BaseColumns {
        public static final String TABLE = "tasks";// Table of the DB
        public static final String COL_TASK_TITLE = "title";// Column in DB
        public static final String COL_CATEGORY_TITLE = "category";// Column in DB
    }

}


