package com.example.calvinusiri.facebooktodoapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbConnectorsHelper extends SQLiteOpenHelper{
    // Helper function that allows for creation of table
    public DbConnectorsHelper(Context context) {
        super(context, DbConnectors.DB_NAME, null, DbConnectors.DB_VERSION);
    }// Constructor

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Query that creates a table "tasks" and columns with that tables
        String createTable = "CREATE TABLE " + DbConnectors.TaskEntry.TABLE + " ( " + DbConnectors.TaskEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DbConnectors.TaskEntry.COL_TASK_TITLE + " TEXT NOT NULL,"+DbConnectors.TaskEntry.COL_CATEGORY_TITLE+ ");";
        db.execSQL(createTable);
    }// Creates new table

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DbConnectors.TaskEntry.TABLE); // drops whole table if it exsists
        onCreate(db);// Creates a new one
    }// Changes drops DB and creates a new one with changes
}
