package com.example.calvinusiri.facebooktodoapp;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calvinusiri.facebooktodoapp.db.DbConnectors;
import com.example.calvinusiri.facebooktodoapp.db.DbConnectorsHelper;

import java.util.ArrayList;

public class TodoActivity extends AppCompatActivity {
    private DbConnectorsHelper dbConnectorsHelper; // Reference To Database models
    private ListView taskListView;// Keeps reference to the ListView in layout activity_todo.xml
    private ArrayAdapter<String> adapter; // Helps to populate ListView with Data from Database
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        dbConnectorsHelper = new DbConnectorsHelper(this);
        taskListView = (ListView) findViewById(R.id.todo_List_id);
        update();// Update method, updates UI With tasks in Database
        genericToast(getString(R.string.welcomeMessage),Toast.LENGTH_LONG);// Informing the use of different use cases
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Menu Item launched to screen because rendered by this method, inflates menu items
        getMenuInflater().inflate(R.menu.menu_item, menu);// Layout of the menu bar in menu/menu_item.xml which is rendered here
        return super.onCreateOptionsMenu(menu);
    }// Menu Item launched to screen because rendered by this method, inflates menu items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Observer for which button was clicked on menu bar
        switch (item.getItemId()) {
            case R.id.menu_item_add_task_id:// Case for Add task button was clicked, this is ID for menu_item.xml
                // Creates Dialogue box layout for user input
                AlertDialog.Builder alert = new AlertDialog.Builder(TodoActivity.this);
                LinearLayout layout = new LinearLayout(TodoActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);
                alert.setTitle(getString(R.string.createTask));
                final EditText taskEditText = new EditText(TodoActivity.this); // Input for task description
                taskEditText.setHint(getString(R.string.addTask));
                layout.addView(taskEditText);
                final EditText categoryEditText = new EditText(TodoActivity.this);// Input for task category
                categoryEditText.setHint(getString(R.string.addCategory));
                layout.addView(categoryEditText);
                alert.setView(layout);
                alert.setPositiveButton(getString(R.string.addDialogueBox), new DialogInterface.OnClickListener() {
                    // Launched when Add Button Is Clicked in Dialogue Box
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String task = String.valueOf(taskEditText.getText());
                        String category = String.valueOf(categoryEditText.getText());
                        if(TextUtils.isEmpty(task)){
                            // Generic toast that string is empty
                            genericToast(getString(R.string.failureToastEnterTask),Toast.LENGTH_SHORT);
                        }
                        else{
                            // Sends User input to sql database
                           SQLiteDatabase db = dbConnectorsHelper.getWritableDatabase();
                           ContentValues values = new ContentValues();
                           values.put(DbConnectors.TaskEntry.COL_TASK_TITLE, task);
                           values.put(DbConnectors.TaskEntry.COL_CATEGORY_TITLE,category);
                           db.insertWithOnConflict(DbConnectors.TaskEntry.TABLE,null, values, SQLiteDatabase.CONFLICT_REPLACE);
                           db.close();
                           update();// Call to update UI List View
                        }
                    }
                });
                alert.setNegativeButton(getString(R.string.cancelDialogueBox), null);
                alert.show();// Show Custom Dialogue box
                return true; // Return boolean type
            case R.id.menu_item_delete_all_id:
                // If delete all button was clicked on nav bar
                deleteAllTasksFromQueue();
                return true;
            case R.id.menu_item_share_id:
                // Case if share button was clicked on nav bar
                getTasksShareThroughEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }// Observer for which button was clicked on menu bar
    private void update(){
        // This method updates the UI with tasks from the database
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = dbConnectorsHelper.getReadableDatabase();
        Cursor cursor = db.query(DbConnectors.TaskEntry.TABLE,
                new String[]{DbConnectors.TaskEntry._ID, DbConnectors.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);
        // Query to get the tasks from the DB
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(DbConnectors.TaskEntry.COL_TASK_TITLE);
            taskList.add(cursor.getString(idx));
        }
        // Uses adapter to help populate the listview
        if (adapter != null) {// IF adapter already exists
            adapter.clear();
            adapter.addAll(taskList);
            adapter.notifyDataSetChanged();
        } else {
            adapter = new ArrayAdapter<>(this,
                    R.layout.row_todo_layout,
                    R.id.task_string_id,
                    taskList);
            taskListView.setAdapter(adapter);
        }
        cursor.close();
        db.close();// Close DB connections
    }// This method updates the UI with tasks from the database
    public void removeTaskFromQueue(View view){
        View parentView = (View) view.getParent(); // Capture reference to parent view
        TextView taskTextView = (TextView) parentView.findViewById(R.id.task_string_id);
        String task = String.valueOf(taskTextView.getText()); // Get the text from the text view
        SQLiteDatabase db = dbConnectorsHelper.getWritableDatabase();
        db.delete(DbConnectors.TaskEntry.TABLE,
                DbConnectors.TaskEntry.COL_TASK_TITLE + " = ?",
                new String[]{task});
        // Make a query for that task from the DB and delete it
        db.close();// Close the db
        update();// Update the UI to reflect the new tasks to be completed
    }// Launched when the completed button is clicked on a task
    private void deleteAllTasksFromQueue(){
        SQLiteDatabase db = dbConnectorsHelper.getWritableDatabase(); // Reference to Db
        db.execSQL("delete from "+DbConnectors.TaskEntry.TABLE );// Query to delete all rows of the Table
        update();// Update the UI to reflect the change in table
    }// Launched when the delete all button is clicked on the nav bar
    private void genericToast(String message, int duration){
        Context context = getApplicationContext();
        Toast.makeText(context,message, duration).show();
    }// Reuse Function For Toasts
    private void getTasksShareThroughEmail(){
        ArrayList<String> taskList = new ArrayList<>();
        String message = "";
        SQLiteDatabase db = dbConnectorsHelper.getReadableDatabase();// Reference to Db
        Cursor cursor = db.query(DbConnectors.TaskEntry.TABLE,
                new String[]{DbConnectors.TaskEntry._ID, DbConnectors.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);
        // Query to get tasks from the DB
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(DbConnectors.TaskEntry.COL_TASK_TITLE);
            taskList.add(cursor.getString(idx));
        }
        for(String x:taskList){
            message = message + x + "\n";
        }// Creates the string that will be the body of the email
        final String sendMessage = message; // Needed To Make It Final Or else could not be referenced within Dialogue
        final EditText sendEmailText = new EditText(this);
        AlertDialog dialog = new AlertDialog.Builder(this) // Dialogue box to get the recipient email
                .setTitle(getString(R.string.emailRecipient))
                .setView(sendEmailText)
                .setPositiveButton(getString(R.string.sendEmailText), new DialogInterface.OnClickListener() {
                    @Override
                    // Launched when the send button is clicked in the dialogue
                    public void onClick(DialogInterface dialog, int which) {
                        String recipient = String.valueOf(sendEmailText.getText());
                        sendEmail(sendMessage,recipient);// Sends the Email
                    }
                })
                .setNegativeButton(getString(R.string.cancelDialogueBox), null)
                .create();
        dialog.show(); // Shows the dialogue
        message = "";
    }// Obtains tasks to send to recipient using sendEmail()
    private void sendEmail(String message,String recipient){
        // Uses Intent to send email to recipient
        if(!TextUtils.isEmpty(message) && !TextUtils.isEmpty(recipient)){
            Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.emailSubject)); // Add subject to email
            intent.putExtra(Intent.EXTRA_TEXT, message);// Body of email
            intent.setData(Uri.parse("mailto:"+recipient)); // or just "mailto:" for blank
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            startActivity(Intent.createChooser(intent, recipient));
        }
        else{
            genericToast(getString(R.string.failureToastEnterEmail),Toast.LENGTH_SHORT);
        }
    }// Using Intent(Send_TO) sends email to recipient
}
